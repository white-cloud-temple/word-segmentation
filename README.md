# Word Segmentation


### Paper

- [中文信息处理中的分词问题](https://gitee.com/white-cloud-temple/word-segmentation/raw/master/paper/word-segmentation-in-chinese-information-processing.pdf) 黄昌宁. 中文信息处理中的分词问题[J]. 语言文字应用(01).
- [中文分词十年回顾](https://gitee.com/white-cloud-temple/word-segmentation/raw/master/paper/chinese-word-segmentation-a-decade-review.pdf) 黄昌宁, 赵海. 中文分词十年回顾[J]. 中文信息学报, 2007, 21(3):8-19.
- [中文分词十年又回顾: 2007-2017*](https://gitee.com/white-cloud-temple/word-segmentation/raw/master/paper/chinese-word-segmentation-another-decade-review-2007-2017.pdf) Zhao H, Cai D, Huang C, et al. Chinese Word Segmentation: Another Decade Review (2007-2017).[J]. arXiv: Computation and Language, 2019.

